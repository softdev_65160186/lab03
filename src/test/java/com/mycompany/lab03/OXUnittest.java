package com.mycompany.lab03;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */

import com.mycompany.lab03.Lab03;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author HP
 */
public class OXUnittest {
    
    public OXUnittest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void testCheckWin_O_Verticel1_output_true() {
        char [][] table = { {'O', 'O', 'O'},{'-', '-', '-'},{'-', '-', '-'}};
        char currentPlayer = 'O';
        boolean result = Lab03.checkWin(table, currentPlayer);
        assertEquals(true,result);
    }
    @Test
    public void testCheckWin_O_Verticel2_output_true() {
        char [][] table = { {'-', '-', '-'},{'O', 'O', 'O'},{'-', '-', '-'}};
        char currentPlayer = 'O';
        boolean result = Lab03.checkWin(table, currentPlayer);
        assertEquals(true,result);
    }
    @Test
    public void testCheckWin_O_Verticel3_output_true() {
        char [][] table = { {'-', '-', '-'},{'-', '-', '-'},{'O', 'O', 'O'}};
        char currentPlayer = 'O';
        boolean result = Lab03.checkWin(table, currentPlayer);
        assertEquals(true,result);
    }
    @Test
    public void testCheckWin_O_Horizontal1_output_true() {
        char [][] table = { {'O', '-', '-'},{'O', '-', '-'},{'O', '-', '-'}};
        char currentPlayer = 'O';
        boolean result = Lab03.checkWin(table, currentPlayer);
        assertEquals(true,result);
    }
    @Test
    public void testCheckWin_O_Horizontal2_output_true() {
        char [][] table = { {'-', 'O', '-'},{'-', 'O', '-'},{'-', 'O', '-'}};
        char currentPlayer = 'O';
        boolean result = Lab03.checkWin(table, currentPlayer);
        assertEquals(true,result);
    }
    @Test
    public void testCheckWin_O_Horizontal3_output_true() {
        char [][] table = { {'-', '-', 'O'},{'-', '-', 'O'},{'-', '-', 'O'}};
        char currentPlayer = 'O';
        boolean result = Lab03.checkWin(table, currentPlayer);
        assertEquals(true,result);
    }
    @Test
    public void testCheckWin_O_X1_output_true() {
        char [][] table = { {'O', '-', '-'},{'-', 'O', '-'},{'-', '-', 'O'}};
        char currentPlayer = 'O';
        boolean result = Lab03.checkWin(table, currentPlayer);
        assertEquals(true,result);
    }
    @Test
    public void testCheckWin_O_X2_output_true() {
        char [][] table = { {'-', '-', 'O'},{'-', 'O', '-'},{'O', '-', '-'}};
        char currentPlayer = 'O';
        boolean result = Lab03.checkWin(table, currentPlayer);
        assertEquals(true,result);
    }
    @Test
    public void testCheckWin_O_Verticel1_output_false() {
        char [][] table = { {'O', 'O', '-'},{'-', '-', '-'},{'-', '-', '-'}};
        char currentPlayer = 'O';
        boolean result = Lab03.checkWin(table, currentPlayer);
        assertEquals(true,result);
    }
    @Test
    public void testCheckWin_O_Verticel2_output_false() {
        char [][] table = { {'-', '-', '-'},{'O', 'O', '-'},{'-', '-', '-'}};
        char currentPlayer = 'O';
        boolean result = Lab03.checkWin(table, currentPlayer);
        assertEquals(true,result);
    }
    @Test
    public void testCheckWin_X_Verticel1_output_true() {
        char [][] table = { {'X', 'X', 'X'},{'-', '-', '-'},{'-', '-', '-'}};
        char currentPlayer = 'X';
        boolean result = Lab03.checkWin(table, currentPlayer);
        assertEquals(true,result);
    }
    @Test
    public void testCheckWin_X_Verticel2_output_true() {
        char [][] table = { {'-', '-', '-'},{'X', 'X', 'X'},{'-', '-', '-'}};
        char currentPlayer = 'X';
        boolean result = Lab03.checkWin(table, currentPlayer);
        assertEquals(true,result);
    }
    @Test
    public void testCheckWin_X_Verticel3_output_true() {
        char [][] table = { {'-', '-', '-'},{'-', '-', '-'},{'X', 'X', 'X'}};
        char currentPlayer = 'X';
        boolean result = Lab03.checkWin(table, currentPlayer);
        assertEquals(true,result);
    }
    @Test
    public void testCheckWin_X_Horizontal1_output_true() {
        char [][] table = { {'X', '-', '-'},{'X', '-', '-'},{'X', '-', '-'}};
        char currentPlayer = 'X';
        boolean result = Lab03.checkWin(table, currentPlayer);
        assertEquals(true,result);
    }
    @Test
    public void testCheckWin_X_Horizontal2_output_true() {
        char [][] table = { {'-', 'X', '-'},{'-', 'X', '-'},{'-', 'X', '-'}};
        char currentPlayer = 'X';
        boolean result = Lab03.checkWin(table, currentPlayer);
        assertEquals(true,result);
    }
    @Test
    public void testCheckWin_X_Horizontal3_output_true() {
        char [][] table = { {'-', '-', 'X'},{'-', '-', 'X'},{'-', '-', 'X'}};
        char currentPlayer = 'X';
        boolean result = Lab03.checkWin(table, currentPlayer);
        assertEquals(true,result);
    }
    @Test
    public void testCheckWin_X_Verticel1_output_false() {
        char [][] table = { {'X', 'X', '-'},{'-', '-', '-'},{'-', '-', '-'}};
        char currentPlayer = 'X';
        boolean result = Lab03.checkWin(table, currentPlayer);
        assertEquals(true,result);
    }
    @Test
    public void testCheckWin_X_Verticel2_output_false() {
        char [][] table = { {'-', '-', '-'},{'X', 'X', '-'},{'-', '-', '-'}};
        char currentPlayer = 'X';
        boolean result = Lab03.checkWin(table, currentPlayer);
        assertEquals(true,result);
    }
}

